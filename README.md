```
        __                                  _____.__        
  ____ |  | __           ____  ____   _____/ ____\__| ____  
 /  _ \|  |/ /  ______ _/ ___\/  _ \ /    \   __\|  |/ ___\ 
(  <_> )    <  /_____/ \  \__(  <_> )   |  \  |  |  / /_/  >
 \____/|__|_ \          \___  >____/|___|  /__|  |__\___  / 
            \/              \/           \/        /_____/  
```

<p>
  <img src="https://img.shields.io/badge/SpringBoot-2.2.5.RELEASE-brightgreen.svg">
  <img src="https://img.shields.io/badge/SpringCloud-Finchley.RELEASE-brightgreen.svg">
</p>

# ok-config

一个很赞的配置中心！

# 地址

http://config.xlbweb.cn

# 组件列表

| 组件 | 版本 |
|----------|----------|
| spring-boot-starter-parent | 2.2.5.RELEASE |
| spring-cloud-config-server | 2.0.0.RELEASE |
| spring-cloud-starter-netflix-eureka-client | 2.0.0.RELEASE |

# 使用说明

```
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-config</artifactId>
</dependency>	    
```

```
#spring.cloud.config.discovery.enabled=true
#spring.cloud.config.discovery.service-id=ok-config
spring.cloud.config.uri=http://config.xlbweb.cn
spring.cloud.config.label=master
spring.cloud.config.profile=dev
spring.cloud.config.name=oa-auth
```

# 部署方法

1、mvn clean package -Dmaven.test.skip=true

2、上传服务器

3、nohup java -jar ok-config.jar --spring.profiles.active=prod >/root/ok-config.log 2>&1 &

# 测试方法

http://localhost:8081

# 在线测试

访问ok-config-repo仓库master分支下oa-auth项目下的oa-oauth-prod.yml配置文件：
- http://config.xlbweb.cn/oa-oauth/prod/master
- http://config.xlbweb.cn/oa-oauth/prod
- curl http://config.xlbweb.cn/oa-oauth/prod

参数说明：

| application | profile | label |
|----------|----------|----------|
| 微服务名称<br/>spring.application.name | 当前环境<br/>dev、test、prod | git分支<br/>master、v1.0、v2.0 默认master |
