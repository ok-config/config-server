package cn.xlbweb.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author: bobi
 * @date: 2019-03-17 18:11
 * @description:
 */
@EnableDiscoveryClient
@EnableConfigServer
@SpringBootApplication
@Slf4j
public class App implements ApplicationRunner {

    @Value("${server.port}")
    public int port;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Tomcat服务启动完成: http://127.0.0.1:{}", port);
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
